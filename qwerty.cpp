﻿#include <iostream>
#include <random>

using namespace std;

const std::string SeedUserName = "Player_With_Score "; // Просто константа для начала имени каждого игрока, чтобы не тратить время на именование. Вынесено для удобства. 

struct Player // Структура игрока. 
{
    std::string name;
    int score;
};


// Это лишь объявление ф-ций. Их описание находится под main().

void Sort(Player*, const int); // Ф-ция самой сортировки.
Player CreateRandomPlayer(); // Ф-ция создания случайного пользователя. Сделано для удобства.
void ShowPlayers(Player*, const int); // Ф-ция отображения данных о массиве игроков. Создано для удобства.


int main()
{
    const int count = 5; // Количество игроков. Можно менять. Но выше X значения компилятор не выделит достаточно памяти для массива. 
    Player* players = new Player[count]; // Выделяем память под игроков. 

    for (int i = 0; i < count; ++i)          // Создаем самих игроков
        players[i] = CreateRandomPlayer();  // Собственно, используя функцию рандома. 

    ShowPlayers(players, count); // Показать игроков перед сортировкой.
    Sort(players, count);        // Отсортировать игроков.
    ShowPlayers(players, count); // Показать игроков после сортировки. 




}

void Sort(Player* players, const int count)
{
    if (count - 1 < 0) return; // Если нечего сортировать - зачем сортировать? Просто выходим.

    bool stop = false; // Флажочек остановки. Когда станет true - сортировать больше нечего. 
    int size = count; // Из-за выбранного типа сортировки - пузырькового - нам понадобится копия "количества игроков", которую можно уменьшать

    while (!stop)
    {
        stop = true;

        for (int i = 0; i < size - 1; ++i)
        {
            if (players[i].score < players[i + 1].score) // Тут все просто. Комментировать думаю не нужно. 
            {
                stop = false;
                std::swap(players[i], players[i + 1]); // А вот тут прокомментирую. В пространстве имен std:: есть полезная функция swap. Наиболее
                // предпочтительный способ свапать элементы массива - именно функция swap.
            }
        }

        size--;

    }

}

Player CreateRandomPlayer()
{
    random_device rd;   // Эти две строки созданы для большей иллюзии машинного псевдорандома. 
    mt19937 gen(rd());  // Так что для вас они пока просто есть.

    Player newPlayer;
    newPlayer.score = gen(); // Рандомная генерация целочисленного числа и его присвоение к значению очков игрока. 
    newPlayer.name = SeedUserName + std::to_string(newPlayer.score); // Константа + очки игрока. Константу можно поменять в начале программы.

    return newPlayer;

}

void ShowPlayers(Player* players, const int count)
{

    std::cout << " < Player Name > " << "         " << " < Player Score > " << std::endl;
    std::cout << "-----------------------------------------------------------------" << std::endl << std::endl;

    for (int i = 0; i < count; ++i)
        std::cout << players[i].name << " : " << players[i].score << endl; // Тут, думаю, комментарии излишни. Просто более-менее красивый вывод значений в консоль.

    std::cout << endl;
    std::cout << "-----------------------------------------------------------------" << std::endl << std::endl;

}


